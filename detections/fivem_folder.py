import os

from settings import DEFAULT_FIVEM_PATH, DEFAULT_FIVEM_EXEC
from miscs import modify_config_json
from translates import translate_detection


def detection_fivem():

    if not os.path.isdir(DEFAULT_FIVEM_PATH):
        print(
            f"===> {translate_detection('system', 'error')} : "
            f"{DEFAULT_FIVEM_PATH} {translate_detection('system', 'not_found')}"
        )

        while True:
            new_fivem_folder_path = input(
                f"{translate_detection('fiveM', 'folder_not_found')} : "
            )
            if os.path.isdir(new_fivem_folder_path):
                modify_config_json("fivem_folder", new_fivem_folder_path)
                break

            print(
                f"===> {translate_detection('system', 'error')} : "
                f"{translate_detection('system', 'bad_path')}"
            )

    if not os.path.isfile(DEFAULT_FIVEM_EXEC):
        print(
            f"===> {translate_detection('system', 'error')} : "
            f"{DEFAULT_FIVEM_EXEC} {translate_detection('system', 'not_found')}"
        )

        while True:
            new_fivem_game_exec = input(
                f"{translate_detection('system', 'error')} : "
                f"{translate_detection('fiveM', 'file_exec_not_found')} : "
            )

            if os.path.isfile(new_fivem_game_exec):
                modify_config_json("fivem_binary", new_fivem_game_exec)
                break

            print(
                f"===> {translate_detection('system', 'error')} : "
                f"{translate_detection('system', 'bad_path')}"
            )

    modify_config_json("modify", "1")
