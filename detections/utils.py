import os

from settings import PATH_CONFIG_JSON
from miscs import read_config_json


def detection_json():
    return os.path.isfile(f"{PATH_CONFIG_JSON}")


def windows_detection():
    if os.name == "nt":
        return True

    raise EnvironmentError


def is_modify():
    if not read_config_json("modify") == "1":
        return False

    return True
