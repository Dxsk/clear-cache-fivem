# FiveM Clear cache 
_Thanks to Andralax and Koxinhell for their help._

### Version : `0.3`

#### Windows Only
    
## Summary :       
- [English](#english)  
- [False Positive](#false-positive)      
- [How to Build](#how-to-build)      
- [Direct download](#direct-download)      
  
  
## Sommaire : 
- [Français](#français)     
- [Faux Positif](#false-positif)   
- [Comment build](#comment-build)      
- [Téléchargement direct](#téléchargement-direct)      
      
### English

"FiveM Clear Cache" is a Windows script for GTA RP players under fiveM.  
The principle is to ease the deletion of the fiveM cache, as well as the execution of the fiveM launcher afterwards.
      
### False Positive  
  
![screen false positive](./screens/false_positive.png)  
 
For a question of transparency, my executable is detected as a virus on some AV (antivirus),
  that's why I provide you the list and the direct link to the detection, moreover I provide you the source code and the way to build the program by the way.
  
  Ps: if you have a solution for that I'll take it :)  
 
[Link to the false positive](https://www.virustotal.com/gui/file/8831d859a5d161d34f80a2086736cce61e9c453c849fc2505fcd759ab41d82b6/detection)  
  
  
### How to build   
      
 `PyInstaller.exe -F -i .\FiveM_cc.ico .\FiveM_cc.py`  
  
### Direct Download  
  
 
- [Version 0.3](https://gitlab.com/Dxsk/clear-cache-fivem/-/tree/0.3)  
- [Version 0.2](https://gitlab.com/Dxsk/clear-cache-fivem/-/tree/0.2)


---  
  
### Français  
  
"FiveM Clear Cache" est un script Windows pour les joueurs de GTA RP sous fiveM.  
Le principe est de faciliter la suppression du cache de fiveM, ainsi que l'exécution du launcher fiveM par la suite.

### False Positive  
  
![image faux positif](./screens/false_positive.png)  
  
  Pour une question de transparence, mon exécutable est détecté en tant que virus sur certains AV (antivirus),
  c'est pour cela que je vous fournis la liste et le lien direct vers la détection, de plus je vous fournis le code source et la manière de build le programme au passage.
  
  Ps: si vous avez une solution pour ça je suis preneur :)
  
[Liens vers le faux positif](https://www.virustotal.com/gui/file/8831d859a5d161d34f80a2086736cce61e9c453c849fc2505fcd759ab41d82b6/detection)  
 

### Comment build  
  `PyInstaller.exe -F -i .\FiveM_cc.ico .\FiveM_cc.py`  
  
### Téléchargement direct  
  
- [Version 0.3](https://gitlab.com/Dxsk/clear-cache-fivem/-/tree/0.3)  
- [Version 0.2](https://gitlab.com/Dxsk/clear-cache-fivem/-/tree/0.2)
