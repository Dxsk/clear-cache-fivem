import json

from settings import PATH_CONFIG_JSON


def read_config_json(entry):

    config_file = open(PATH_CONFIG_JSON, "r", encoding="utf-8")
    config_file_json = json.load(config_file)
    return config_file_json[f"{entry}"]
