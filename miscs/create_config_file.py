import json

from settings import DEFAULT_FIVEM_EXEC, DEFAULT_FIVEM_PATH, DEFAULT_FIVEM_IGNORE_FOLDER
from translates import translate_detection


def create_config_file():

    print(f"===> {translate_detection('system', 'create')} config.json")

    data = {
        "fivem_folder": f"{DEFAULT_FIVEM_PATH}",
        "fivem_binary": f"{DEFAULT_FIVEM_EXEC}",
        "fivem_ignore_folder": f"{DEFAULT_FIVEM_IGNORE_FOLDER}",
        "modify": "0",
    }

    config_file = open("config.json", "w+")
    config_file.write(json.dumps(data, indent=4))
    config_file.close()
