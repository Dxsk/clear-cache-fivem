import json


def modify_config_json(entry, data):

    with open("config.json", "r+") as file:
        d = json.loads(file.read())
        d[f"{entry}"] = data
        file.seek(0)
        json.dump(d, file, indent=4)
        file.truncate()
