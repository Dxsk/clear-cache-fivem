import os
import json
import shutil

from settings import PATH_CONFIG_JSON
from translates import translate_detection


def clear_cache():

    config_file = open(PATH_CONFIG_JSON, "r", encoding="utf-8")
    config_file_json = json.load(config_file)
    target_folder = config_file_json["fivem_folder"]
    ignore_folder = config_file_json["fivem_ignore_folder"]
    config_file.close()

    print(f"===> {translate_detection('system', 'run_clear_cache')}")

    for element in os.listdir(target_folder):

        if not element == ignore_folder:

            if os.path.isfile(os.path.join(target_folder, element)):
                os.remove(os.path.join(target_folder, element))
            else:
                shutil.rmtree(os.path.join(target_folder, element))
