import os
import json

from settings import PATH_CONFIG_JSON
from translates import translate_detection


def run_game():
    config_file = open(PATH_CONFIG_JSON, "r", encoding="utf-8")
    config_file_json = json.load(config_file)
    target_game_exec = config_file_json["fivem_binary"]
    print(f"{target_game_exec} {translate_detection('system', 'run')}")
    os.system(target_game_exec)
