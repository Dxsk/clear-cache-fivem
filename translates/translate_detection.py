from locale import getdefaultlocale

from .available_translation import available_translation
from .languages import languages


def translate_detection(entry_type, entry_word):
    languages_lists = languages()
    language, _ = getdefaultlocale()
    language = available_translation(language)

    if language is None:
        language = available_translation("en_US")

    datas = languages_lists[f"{language}"]
    return datas[entry_type][entry_word]
