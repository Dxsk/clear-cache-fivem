def languages():
    return {
        "en": {
            "system": {
                "error": "ERROR",
                "only_windows": "ERROR : This program only works on Windows",
                "not_found": "Not found !",
                "bad_path": "Please indicate a real path!",
                "run": "Is running ...",
                "create": "Creating ",
                "run_clear_cache": "Clear Cache is launching",
            },
            "fiveM": {
                "folder_not_found": "Please write down the complete path for FiveM's folder",
                "file_exec_not_found": "Please write down the complete path of the FiveM's executable / .exe",
            },
        },
        "fr": {
            "system": {
                "error": "ERREUR",
                "only_windows": "ERREUR : Ce programme fonctionne uniquement sur windows",
                "not_found": "non trouvé !",
                "bad_path": "Veuillez indiquer un chemin réel !",
                "run": "se lance ...",
                "create": "Création de",
                "run_clear_cache": "Lancement du clear cache",
            },
            "fiveM": {
                "folder_not_found": "Veuillez indiquer le chemin complet du dossier FIVEM",
                "file_exec_not_found": "Veuillez indiquer le chemin complet du .exe de FIVEM",
            },
        },
    }
