def available_translation(language):

    lists = {"fr_FR": "fr", "en_US": "en"}

    return lists.get(language)
