from detections import detection_json, detection_fivem, is_modify
from miscs import create_config_file
from translates import translate_detection


def initializer():
    print(f"===> initializer {translate_detection('system', 'run')}")

    if not detection_json():
        print(f"===> config.json {translate_detection('system', 'not_found')}")
        create_config_file()

    if detection_json():

        if not is_modify():
            detection_fivem()
