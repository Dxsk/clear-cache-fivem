from sys import exit

from detections import windows_detection
from bootloader import loader
from initializer import initializer
from miscs import clear_cache, run_game
from translates import translate_detection


def main():
    loader()
    initializer()
    clear_cache()
    run_game()
    exit(0)


if __name__ == "__main__":
    try:
        windows_detection()

    except EnvironmentError:
        print(translate_detection("system", "only_windows"))
        exit(1)

    else:
        main()
