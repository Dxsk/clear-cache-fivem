import os
import getpass

DEFAULT_FIVEM_PATH = (
    f"C:\\Users\\{getpass.getuser()}\\AppData\\Local\\FiveM\\FiveM.app\\cache"
)
DEFAULT_FIVEM_EXEC = f"C:\\Users\\{getpass.getuser()}\\AppData\\Local\\FiveM\\FiveM.exe"
DEFAULT_FIVEM_IGNORE_FOLDER = "games"

PATH_SCRIPT = os.getcwd()
PATH_CONFIG_JSON = f"{PATH_SCRIPT}\\config.json"
